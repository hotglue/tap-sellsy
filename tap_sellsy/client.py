"""REST client handling, including SellsyStream base class."""

from pathlib import Path
from time import time
from typing import Any, Dict, Iterable, List, Optional, Union

import requests
from memoization import cached
from singer_sdk.authenticators import BearerTokenAuthenticator
from singer_sdk.helpers.jsonpath import extract_jsonpath
from singer_sdk.streams import RESTStream

SCHEMAS_DIR = Path(__file__).parent / Path("./schemas")


class SellsyStream(RESTStream):
    """Sellsy stream class."""

    expires_in = None
    access_token = None
    _page_size = 25
    offset = 0

    url_base = "https://api.sellsy.com/v2"

    records_jsonpath = "$.data[*]"
    next_page_token_jsonpath = "$.pagination"

    def get_token(self):
        expires_in = self.expires_in
        now = round(time())
        if not self.access_token or (not expires_in) or ((expires_in - now) < 60):
            s = requests.Session()
            payload = {
                "grant_type": "client_credentials",
                "client_id": self.config.get("client_id"),
                "client_secret": self.config.get("client_secret"),
            }
            login = s.post(
                "https://login.sellsy.com/oauth2/access-tokens", json=payload
            ).json()
            self.access_token = login["access_token"]
            self.expires_in = login["expires_in"]
        return self.access_token

    @property
    def authenticator(self) -> BearerTokenAuthenticator:
        """Return a new authenticator object."""
        if (
            self.config.get("client_id")
            and self.config.get("client_secret") is not None
        ):
            token = self.get_token()

        return BearerTokenAuthenticator.create_for_stream(self, token=token)

    @property
    def http_headers(self) -> dict:
        """Return the http headers needed."""
        headers = {}
        if "user_agent" in self.config:
            headers["User-Agent"] = self.config.get("user_agent")
        return headers

    def get_next_page_token(
        self, response: requests.Response, previous_token: Optional[Any]
    ) -> Optional[Any]:

        data = response.json()
        if "data" not in data:
            return None

        if len(data["data"]) == 0:
            return None

        if self.next_page_token_jsonpath:
            all_matches = extract_jsonpath(
                self.next_page_token_jsonpath, response.json()
            )
            first_match = next(iter(all_matches), None)
            next_page_token = first_match
        else:
            next_page_token = response.headers.get("X-Next-Page", None)

        if "offset" in next_page_token:
            self.offset = next_page_token["offset"]

        return next_page_token

    def get_url_params(
        self, context: Optional[dict], next_page_token: Optional[Any]
    ) -> Dict[str, Any]:
        params: dict = {}
        if next_page_token:
            params["offset"] = self.offset

        return params
