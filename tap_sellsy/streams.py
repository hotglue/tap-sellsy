"""Stream type classes for tap-sellsy."""

from pathlib import Path
from typing import Optional

from singer_sdk import typing as th  # JSON Schema typing helpers

from tap_sellsy.client import SellsyStream


class CompaniesStream(SellsyStream):
    """Define custom stream."""

    name = "companies"
    path = "/companies"
    primary_keys = ["id"]
    replication_key = None

    schema = th.PropertiesList(
        th.Property("id", th.NumberType),
        th.Property("name", th.StringType),
        th.Property("type", th.StringType),
        th.Property("name", th.StringType),
        th.Property("email", th.StringType),
        th.Property("website", th.StringType),
        th.Property("phone_number", th.StringType),
        th.Property("mobile_number", th.StringType),
        th.Property("fax_number", th.StringType),
        th.Property("legal_france", th.CustomType({"type": ["object", "string"]})),
        th.Property("capital", th.StringType),
        th.Property("reference", th.StringType),
        th.Property("note", th.StringType),
        th.Property("auxiliary_code", th.StringType),
        th.Property("social", th.CustomType({"type": ["object", "string"]})),
        th.Property("rate_category_id", th.NumberType),
        th.Property("accounting_code_id", th.NumberType),
        th.Property("main_contact_id", th.NumberType),
        th.Property("dunning_contact_id", th.NumberType),
        th.Property("invoicing_contact_id", th.NumberType),
        th.Property("invoicing_address_id", th.NumberType),
        th.Property("delivery_address_id", th.NumberType),
        th.Property("accounting_purchase_code_id", th.NumberType),
        th.Property("owner", th.CustomType({"type": ["object", "string"]})),
        th.Property("is_archived", th.BooleanType),
        th.Property("business_segment", th.CustomType({"type": ["object", "string"]})),
        th.Property(
            "number_of_employees", th.CustomType({"type": ["object", "string"]})
        ),
        th.Property(
            "marketing_campaigns_subscriptions",
            th.CustomType({"type": ["array", "string"]}),
        ),
        th.Property("_embed", th.CustomType({"type": ["object", "string"]})),
        th.Property("created", th.DateTimeType),
        th.Property("updated_at", th.DateTimeType),
    ).to_dict()


class ItemsStream(SellsyStream):
    name = "items"
    path = "/items"
    primary_keys = ["id"]
    replication_key = None
    schema = th.PropertiesList(
        th.Property("id", th.NumberType),
        th.Property("type", th.StringType),
        th.Property("name", th.StringType),
        th.Property("reference", th.StringType),
        th.Property("reference_price", th.StringType),
        th.Property("reference_price_taxes_exc", th.StringType),
        th.Property("reference_price_taxes_inc", th.StringType),
        th.Property("is_reference_price_taxes_free", th.BooleanType),
        th.Property("tax_id", th.NumberType),
        th.Property("unit_id", th.NumberType),
        th.Property("price_excl_tax", th.StringType),
        th.Property("currency", th.StringType),
        th.Property("standard_quantity", th.StringType),
        th.Property("description", th.StringType),
        th.Property("is_name_included_in_description", th.BooleanType),
        th.Property("accounting_code_id", th.NumberType),
        th.Property("accounting_purchase_code_id", th.NumberType),
        th.Property("is_archived", th.BooleanType),
    ).to_dict()


class InvoicesStream(SellsyStream):
    """Define custom stream."""

    name = "invoices"
    path = "/invoices"
    primary_keys = ["id"]
    replication_key = None
    schema = th.PropertiesList(
        th.Property("id", th.NumberType),
        th.Property("number", th.StringType),
        th.Property("status", th.StringType),
        th.Property("date", th.DateType),
        th.Property("due_date", th.DateType),
        th.Property("created", th.DateTimeType),
        th.Property("subject", th.StringType),
        th.Property("amounts", th.CustomType({"type": ["object", "string"]})),
        th.Property("currency", th.StringType),
        th.Property("taxes", th.CustomType({"type": ["array", "string"]})),
        th.Property("discount", th.CustomType({"type": ["object", "string"]})),
        th.Property("related", th.CustomType({"type": ["array", "string"]})),
        th.Property("public_link", th.CustomType({"type": ["object", "string"]})),
        th.Property(
            "payment_conditions_acceptance",
            th.CustomType({"type": ["object", "string"]}),
        ),
        th.Property("owner", th.CustomType({"type": ["object", "string"]})),
        th.Property("fiscal_year_id", th.NumberType),
        th.Property("pdf_link", th.StringType),
        th.Property("decimal_number", th.CustomType({"type": ["object", "string"]})),
        th.Property("assigned_staff_id", th.NumberType),
        th.Property("contact_id", th.NumberType),
        th.Property("invoicing_address_id", th.NumberType),
        th.Property("delivery_address_id", th.NumberType),
        th.Property("_embed", th.CustomType({"type": ["object", "string"]})),
    ).to_dict()

    def get_child_context(self, record: dict, context: Optional[dict]) -> dict:

        return {
            "invoice_id": record["id"],
        }


class InvoiceItemssStream(SellsyStream):
    """Define custom stream."""

    name = "invoice_items"
    path = "/invoices/{invoice_id}"
    primary_keys = ["id"]
    parent_stream_type = InvoicesStream
    replication_key = None
    records_jsonpath = "$[*]"
    schema = th.PropertiesList(
        th.Property("id", th.NumberType),
        th.Property("number", th.StringType),
        th.Property("status", th.StringType),
        th.Property("date", th.DateType),
        th.Property("due_date", th.DateType),
        th.Property("created", th.DateTimeType),
        th.Property("subject", th.StringType),
        th.Property("amounts", th.CustomType({"type": ["object", "string"]})),
        th.Property("currency", th.StringType),
        th.Property("taxes", th.CustomType({"type": ["array", "string"]})),
        th.Property("discount", th.CustomType({"type": ["object", "string"]})),
        th.Property("related", th.CustomType({"type": ["array", "string"]})),
        th.Property("public_link", th.CustomType({"type": ["object", "string"]})),
        th.Property(
            "payment_conditions_acceptance",
            th.CustomType({"type": ["object", "string"]}),
        ),
        th.Property("owner", th.CustomType({"type": ["object", "string"]})),
        th.Property("fiscal_year_id", th.NumberType),
        th.Property("pdf_link", th.StringType),
        th.Property("decimal_number", th.CustomType({"type": ["object", "string"]})),
        th.Property("assigned_staff_id", th.NumberType),
        th.Property("contact_id", th.NumberType),
        th.Property("invoicing_address_id", th.NumberType),
        th.Property("delivery_address_id", th.NumberType),
        th.Property("_embed", th.CustomType({"type": ["object", "string"]})),
        th.Property("rows", th.CustomType({"type": ["array", "string"]})),
    ).to_dict()


class OCRStream(SellsyStream):
    """Define custom stream."""

    name = "ocr_purchase_invoices"
    path = "/ocr/pur-invoice"
    primary_keys = ["id"]
    replication_key = None
    schema = th.PropertiesList(
        th.Property("id", th.NumberType),
        th.Property("staff_id", th.NumberType),
        th.Property("file_name", th.StringType),
        th.Property("linked_type", th.StringType),
        th.Property("linked_id", th.NumberType),
        th.Property("created_at", th.DateTimeType),
        th.Property("completed_at", th.DateTimeType),
        th.Property("state", th.StringType),
        th.Property("error_code", th.NumberType),
    ).to_dict()
