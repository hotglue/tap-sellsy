"""Sellsy tap class."""

from typing import List

from singer_sdk import Stream, Tap
from singer_sdk import typing as th


from tap_sellsy.streams import (
    CompaniesStream,
    InvoiceItemssStream,
    InvoicesStream,
    ItemsStream,
    OCRStream,
    SellsyStream,
)


STREAM_TYPES = [
    CompaniesStream,
    ItemsStream,
    InvoicesStream,
    OCRStream,
    InvoiceItemssStream,
]


class TapSellsy(Tap):
    """Sellsy tap class."""

    name = "tap-sellsy"

    config_jsonschema = th.PropertiesList(
        th.Property(
            "client_id",
            th.StringType,
            required=True,
        ),
        th.Property(
            "client_secret",
            th.StringType,
            required=True,
        ),
    ).to_dict()

    def discover_streams(self) -> List[Stream]:
        """Return a list of discovered streams."""
        return [stream_class(tap=self) for stream_class in STREAM_TYPES]


if __name__ == "__main__":
    TapSellsy.cli()
